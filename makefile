SRCDIR=./src/
BINDIR=./bin/
OBJDIR=$(BINDIR)obj
EXEDIR=$(BINDIR)exe
EXEFILE=chc
FLAGS=-c -I .
DEBUGFLAGS=$(FLAGS) -g -Wall

all:
	gcc $(SRCDIR)main.c $(FLAGS)
	gcc $(SRCDIR)compression.c $(FLAGS)
	gcc $(SRCDIR)expansion.c $(FLAGS)
	echo Created Object Files From .c Files
	gcc main.o compression.o expansion.o -o $(EXEFILE)
	echo Created Executable File
	mkdir -p $(BINDIR)
	mkdir -p $(OBJDIR) $(EXEDIR)
	echo Made Directories for Built Files
	mv *.o $(OBJDIR)
	mv chc $(EXEDIR)
	echo Moved Built Files To The Appropriate Directory

clean:
	rm -rf $(BINDIR)
	echo Made Clean

debug:
	gcc $(SRCDIR)main.c $(DEBUGFLAGS)
	gcc $(SRCDIR)compression.c $(DEBUGFLAGS)
	gcc $(SRCDIR)expansion.c $(DEBUGFLAGS)
	echo Created Object Files From .c Files
	gcc main.o compression.o expansion.o -o $(EXEFILE) -g
	echo Created Executable File
	mkdir -p $(BINDIR)
	mkdir -p $(OBJDIR) $(EXEDIR)
	echo Made Directories for Built Files
	mv *.o $(OBJDIR)
	mv chc $(EXEDIR)
	echo Moved Built Files To The Appropriate Directory

rebuild:
	make clean
	make all