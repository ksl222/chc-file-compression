# Overview
- This is a basic program that can losslessly compress (and expand) files.
- The program was created for Linux operating systems, but will be updated in the future for Windows.
- File compressed with this program end the the ".chc" file extension.
- Although it will work on large files (> a few MB), it may take a while, especially for the expansion.

# Program Building
- To build the program, enter the command "make".

# Program Usage

## File Compression
- Use the command "./bin/exe/chc -c InputFileName" to compress the file, InputFileName.
- Use the "-o FileName" flag and argument to change the compressed file name to FileName.
## File Expansion
- Use the command "./bin/exe/chc -e InputFileName" to expand a compressed file.
- Use the "-o Path/To/Output/Directory" flag and argument to change the directory where the expanded output file should be placed.

# File Cleaning
- To clean the object and executable files, use the command "make clean".

# Program Debugging Info
- For no debug information to be printed, change the "debugMessages" macro in the lib.h file to 0.
- For some debug information to be printed, change the "debugMessages" macro in the lib.h file to 1.
- For extra debug information to be printed, change the "debugMessages" macro in the lib.h file from 2.
- (The object and executable files must be rebuilt after the debugMessages is changed in order for the change to take effect.)
- To build the object and executable files with debugging symbols, use the command "make debug".

# Misc
- In order to remove and rebuild the object and executable files, use the command "make rebuild".
