#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//Set debugMessages to 1 for debug info or 2 for extra debug info
#define debugMessages 0

//Compression file functions

typedef struct{
	struct Node* left;
	struct Node* right;
	unsigned int count;
	char* bitString;
	char isLeaf;
	unsigned char c;
}Node;

void compress(char* inputFile, char* outputFILE);

void initNode(Node* node);

unsigned int numElements(unsigned int* list);

void getBitStrings(Node* node);

void selectionSort(Node** list, unsigned int listLength);

unsigned int numNodes(Node** nodeList, unsigned int numNodesInRow);

void treeToTable(Node* node, Node** list);

void writeToBuff(FILE* output, char* str);

//Expansion file contents

typedef struct{
	unsigned char c;
	unsigned char codeLength;
	unsigned long code;
}SNode;

void expansion(char* inputFileName, char* outputFilePath);