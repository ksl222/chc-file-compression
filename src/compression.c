#include "functions.h"

unsigned int listIndex;
unsigned int stringLength;
char* bitString;
unsigned char buff[8];
char nextByte;
unsigned char bitLength;

void initNode(Node* node){
	node->left = NULL;
	node->right = NULL;
	node->count = 0;
	node->isLeaf = 1;
	node->c = 0;
	node->bitString = NULL;
}

unsigned int numElements(unsigned int* list){
	unsigned int k = 0;
	for(int i = 0; i < 256; i++){
		if(list[i] != 0) k++;
	}
	return k;
}

unsigned int numNodes(Node** nodeList, unsigned int numNodesInRow){
	unsigned int count = 0;
	for(int i = 0; i < numNodesInRow; i++){
		if(nodeList[i] != NULL) count++;
	}
	return count;
}

void compress(char* inputFileName, char* outputFileName){
	if(debugMessages) printf("DEBUG: Started to compress input file\n");
	unsigned int** charCount = (unsigned int**) malloc(2048);
	for(int i = 0; i < 256; i++) charCount[i] = NULL;
	FILE* inputFile = fopen(inputFileName, "r");

	//Count number of occurences of each character in input file
	unsigned char prev = fgetc(inputFile);
	unsigned char next = fgetc(inputFile);
	unsigned int numDiffChars = 0;
	while(feof(inputFile) == 0){
		//printf("prev: %02x\tnext: %02x\n", prev, next);
		if(charCount[prev] == NULL){
			charCount[prev] = (unsigned int*) malloc(2048);
			for(int i = 0; i < 256; i++) charCount[prev][i] = 0;
			numDiffChars++;
			//printf("\tNumber of Different Characters: %02x\n", numDiffChars);
		}
		charCount[prev][next]++;
		prev = next;
		next = fgetc(inputFile);
	}
	if(numDiffChars == 0){
		printf("ERROR: Input file is empty. Compression aborted.\n");
		return 1;
	}
	if(debugMessages){
		printf("DEBUG: Finished counting characters\n");
		/*for(unsigned int i = 0; i < 256; i++){
			if(charCount[i] != NULL){
				printf("%02x\n", (char)i);
				for(unsigned int j = 0; j < 256; j++){
					if(charCount[i][j] != 0) printf("\t%x: 0x%x\n", (char)j, charCount[i][j]);
				}
			}
		}*/
		printf("Num Diff Chars: %02x\n", numDiffChars);
	}

	unsigned char* diffChars = malloc(numDiffChars);
	unsigned int* numCharsInRow = malloc(numDiffChars*4);
	unsigned long fileSize = (unsigned int)ftell(inputFile);

	fclose(inputFile);

	//Create the table of characters and their occurrences
	Node*** table = (Node***) malloc(8*numDiffChars);

	unsigned int tableIndex = 0;
	unsigned int rowIndex = 0;
	unsigned int charIndex = 0;
	for(unsigned int i = 0; i < 256; i++){
		if(charCount[i] != NULL){
			diffChars[charIndex] = (char)i;
			unsigned int num = numElements(charCount[i]);
			//if(debugMessages) printf("DEBUG: Table\t%02x: %i\n", (char)i, num);
			numCharsInRow[charIndex++] = num;
			table[tableIndex] = (Node**) malloc(8*num);
			for(int j = 0; j < 256; j++){
				if(charCount[i][j] > 0){
					Node* node = (Node*) malloc(sizeof(Node));
					table[tableIndex][rowIndex] = node;
					initNode(node);
					node->c = (char)j;
					node->count = charCount[i][j];
					rowIndex++;
				}
				if(rowIndex == num) break;
			}
			free(charCount[i]);
			tableIndex++;
			rowIndex = 0;
		}
		if(tableIndex == numDiffChars) break;
	}

	free(charCount);

	if(debugMessages){
		printf("DEBUG: Finished building node table\n");
		/*for(int i = 0; i < numDiffChars; i++){
			printf("%c\n", diffChars[i]);
			for(int j = 0; j < numCharsInRow[i]; j++){
				printf("\t%c: %x\n", table[i][j]->c, table[i][j]->count);
			}
		}*/
	}

	//Turn each row of the character table into a binary tree
	Node** rootNodes = (Node**) malloc(8*numDiffChars);
	for(unsigned int i = 0; i < numDiffChars; i++){
		Node* root = table[i][0];
		if(numCharsInRow[i] > 1){
			while(numNodes(table[i], numCharsInRow[i]) > 1){
				Node* min1 = NULL;
				Node* min2 = NULL;
				unsigned char index1 = 0;
				unsigned char index2 = 0;
				for(int j = 0; j < numCharsInRow[i]; j++){
					if(min1 == NULL){
						min1 = table[i][j];
						index1 = j;
					} 
					else if(min2 == NULL){
						min2 = table[i][j];
						index2 = j;
					} 
					else if(table[i][j] != NULL){
						unsigned int count = table[i][j]->count;
						unsigned int count1 = min1->count;
						unsigned int count2 = min2->count;
						if((count <= count1) && (count1 >= count2)){
							min1 = table[i][j];
							index1 = j;
						}
						else if((count <= count2) && (count2 >= count1)){
							min2 = table[i][j];
							index2 = j;
						}
					}
				}
				Node* newNode = (Node*) malloc(sizeof(Node));
				initNode(newNode);
				newNode->isLeaf = 0;
				newNode->count = min1->count + min2->count;
				newNode->left = min1;
				newNode->right = min2;
				table[i][index1] = newNode;
				table[i][index2] = NULL;
				root = newNode;
			}
		}
		rootNodes[i] = root;
	}

	if(debugMessages) printf("DEBUG: Finished building node trees\n");

	//Create the Huffman codes for each character
	bitString = malloc(numDiffChars);
	memset(bitString, 0, numDiffChars);
	stringLength = 0;

	for(unsigned int i = 0; i < numDiffChars; i++) getBitStrings(rootNodes[i]);

	if(debugMessages) printf("DEBUG: Finished creating character codes\n");

	for(unsigned int i = 0; i < numDiffChars; i++){
		listIndex = 0;
		treeToTable(rootNodes[i], table[i]);
	}

	free(rootNodes);

	//Turn the character trees back into lists of characters
	if(debugMessages){
		printf("DEBUG: Finished converting trees to table\n");
		/*for(int i = 0; i < numDiffChars; i++){
			printf("%c\n", diffChars[i]);
			for(int j = 0; j < numCharsInRow[i]; j++){
				printf("\t%c: %s\n", table[i][j]->c, table[i][j]->bitString);
			}
		}*/
	}

	//Sort the nodes in each row of the character table by the length of the code
	for(unsigned int i = 0; i < numDiffChars; i++) selectionSort(table[i], numCharsInRow[i]);

	if(debugMessages) printf("DEBUG: Finished sorting nodes\n");

	//Convert the Huffman codes into canonical Huffman codes
	for(unsigned int i = 0; i < numDiffChars; i++){
		//printf("In row %i\n", i);
		memset(bitString, 0, numDiffChars);
		memset(bitString, '0', strlen(table[i][0]->bitString));
		memset(table[i][0]->bitString, '0', strlen(table[i][0]->bitString));

		for(unsigned int j = 1; j < numCharsInRow[i]; j++){
			//printf("\tConverting code %i\n", j);
			unsigned int k = strlen(bitString)-1;
			while(k >= 0){
				if(bitString[k] == '0'){
					bitString[k] = '1';
					break;
				}
				else{
					bitString[k] = '0';
					k--;
				}
			}
			//printf("\t\tAdded 1\n");
			while(strlen(bitString) < strlen(table[i][j]->bitString)) bitString[strlen(bitString)] = '0';
			//printf("\t\tShifted code left\n");
			memcpy(table[i][j]->bitString, bitString, strlen(bitString));
		}
	}

	if(debugMessages){
		printf("DEBUG: Finished converting codes to canonical Huffman codes\n");
		for(unsigned int i = 0; i < numDiffChars; i++){
			printf("%x\n", diffChars[i]);
			for(unsigned int j = 0; j < numCharsInRow[i]; j++){
				printf("\t%x: %s\n", table[i][j]->c, table[i][j]->bitString);
			}
		}
	}

	FILE* outputFile = fopen(outputFileName, "w");


	//Find file name not including path
	char* actualFileName;
	for(int i = 0; i < strlen(inputFileName); i++) if(inputFileName[i] == '/') actualFileName = &inputFileName[i];
	actualFileName++;

	//Write length of file name
	unsigned char l = (char)strlen(actualFileName);
	fputc(l, outputFile);
	//Write file name
	for(int i = 0; i < l; i++){
		fputc(actualFileName[i], outputFile);
	}
	//Write length of file
	for(int i = 7; i >= 0; i--){
		fputc((char)((fileSize >> (8*i)) & (0xFF)), outputFile);
	}
	if(debugMessages) printf("DEBUG: Finished writing file meta-data\n");

	//Write number of rows
	fputc((char)(numDiffChars-1), outputFile);
	//Write table
	for(int i = 0; i < numDiffChars; i++){
		fputc(diffChars[i], outputFile);
		fputc((char)(numCharsInRow[i]-1), outputFile);
		for(int j = 0; j < numCharsInRow[i]; j++){
			fputc(table[i][j]->c, outputFile);
			fputc(strlen(table[i][j]->bitString), outputFile);
		}
	}

	if(debugMessages) printf("DEBUG: Finished writing key table\n");

	memset(buff, 0, 8);
	nextByte = 0;
	bitLength = 0;
	inputFile = fopen(inputFileName, "r");
	prev = fgetc(inputFile);
	fputc(prev, outputFile);
	next = fgetc(inputFile);
	//unsigned char temp = 240;
	while(feof(inputFile) == 0){
		//printf("File Length: %x\n", --fileSize);
		unsigned int index1 = 0;
		while(diffChars[index1] != prev) index1++;
		//printf("\tIndex 1: %i\n", index1);
		unsigned int index2 = 0;
		while(table[index1][index2]->c != next) index2++;
		//printf("\tIndex 2: %i\n", index2);
		//printf("%02x : %02x : %s\n", prev, next, table[index1][index2]->bitString);
		/*if(debugMessages && (temp > 0)){
			printf("%c : %c: %s\n", prev, next, table[index1][index2]->bitString);
			temp--;
		}*/
		//printf("\t%s\n", table[index1][index2]->bitString);
		writeToBuff(outputFile, table[index1][index2]->bitString);
		prev = next;
		next = fgetc(inputFile);
	}
	fclose(inputFile);

	if(debugMessages) printf("DEBUG: Finished writing compressed file\n");

	char k = 0;
	for(int i = 0; i < 8; i++){
		if(buff[i] != 0) k = (k << 1) + buff[i] - '0';
		else k = k << 1;
	}
	fputc(k, outputFile);
	fclose(outputFile);

	if(debugMessages) printf("DEBUG: Finished writing final byte with padding\n");

	for(int i = 0; i < numDiffChars; i++){
		for(int j = 0; j < numCharsInRow[i]; j++){
			free(table[i][j]->bitString);
			free(table[i][j]);
		}
		free(table[i]);
	}
	free(table);

	if(debugMessages) printf("DEBUG: Freed table\n");
	free(bitString);
	//printf("\tFreed bit string\n");
	free(numCharsInRow);
	//printf("\tFreed num chars in row\n");
	free(diffChars);
	if(debugMessages) printf("DEBUG: Freed misc.\n");
}


void writeToBuff(FILE* output, char* str){
	while(*str != 0){
		nextByte = nextByte << 1;
		if(*str == '1') nextByte++;
		bitLength++;
		if(bitLength == 8){
			fputc(nextByte, output);
			nextByte = 0;
			bitLength = 0;
		}
		str++;
	}
}

void selectionSort(Node** list, unsigned int listLength){
	if(listLength < 2) return;
	for(unsigned int i = 0; i < listLength; i++){
		Node* smallest = list[i];
		unsigned char index = i;
		for(int j = i+1; j < listLength; j++){
			if(strlen(list[j]->bitString) < strlen(smallest->bitString)){
				smallest = list[j];
				index = j;
			}
		}
		list[index] = list[i];
		list[i] = smallest;
	}
}

void treeToTable(Node* node, Node** list){
	if(node->isLeaf == 1){
		list[listIndex++] = node;
	}
	else{
		treeToTable(node->left, list);
		treeToTable(node->right, list);
		free(node);
	}
}

void getBitStrings(Node* node){
	if(node->isLeaf == 1){
		node->bitString = malloc(stringLength);
		strcpy(node->bitString, bitString);
	}
	else{
		bitString[stringLength] = '0';
		++stringLength;
		getBitStrings(node->left);
		bitString[stringLength-1] = '1';
		getBitStrings(node->right);
		bitString[--stringLength] = 0;
	}
}