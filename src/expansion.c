#include "functions.h"


void expansion(char* inputFileName, char* outputFilePath){
	FILE* inputFile = fopen(inputFileName, "r");

	//Get output file name
	unsigned char fileNameSize = fgetc(inputFile);
	unsigned char pathLength = strlen(outputFilePath);
	unsigned char outputFileName[fileNameSize+pathLength+1];
	strcpy(outputFileName, outputFilePath);
	if(pathLength > 0) {
		outputFileName[pathLength] = '/';
		for(int i = 0; i < fileNameSize; i++){
			outputFileName[i+pathLength+1] = fgetc(inputFile);
		}
		pathLength++;
	}
	else {
		*outputFileName = &outputFileName[2];
		for(int i = 0; i < fileNameSize; i++){
			outputFileName[i] = fgetc(inputFile);
		}
	}
	outputFileName[fileNameSize+pathLength] = 0;
	if(debugMessages) printf("DEBUG: File name found, \"%s\"\n", outputFileName);
	FILE* outputFile = fopen(outputFileName, "r");
	if(outputFile != NULL){
			printf("WARNING: Output file, \"%s\", already exists. If you continue it will be overwritten.\n", outputFileName);
			fclose(outputFile);
			printf("Would you like to proceed anyway? (y/n)\t");
			int k = getc(stdin);
			if(k == 'n'){
				printf("\nExpansion aborted.\n");
				return;
			}
			else if(k == 'y'){
				printf("\nExpansion continuing.\n");
			}
			else{
				while((k != 'y') && (k != 'n')){
					printf("\nERROR: Invalid option selected.\n");
					printf("Would you like to proceed anyway? (y/n)\t");
					k = getc(stdin);
				}
				if(k == 'n'){
				printf("\nExpansion aborted.\n");
				return;
				}
				else if(k == 'y'){
					printf("\nExpansion continuing.\n");
				}
			}
		}
	//Get expanded file size
	unsigned long fileSize = fgetc(inputFile);
	for(int i = 0; i < 7; i++) fileSize = (fileSize << 8) + fgetc(inputFile);

	if(debugMessages) printf("DEBUG: File size found, %x bytes\n", fileSize);

	unsigned int numDiffChars = fgetc(inputFile)+1;

	//Get key table from file
	SNode*** table = (SNode***) malloc(8*numDiffChars);
	unsigned int* numCharsInRow = (unsigned int*) malloc(numDiffChars*4);
	unsigned char* diffChars = (unsigned char*) malloc(numDiffChars);
	for(int i = 0; i < numDiffChars; i++){
		diffChars[i] = fgetc(inputFile);
		numCharsInRow[i] = (unsigned int)(fgetc(inputFile)+1);
		table[i] = (SNode**) malloc(8*numCharsInRow[i]);
		for(int j = 0; j < numCharsInRow[i]; j++){
			table[i][j] = (SNode*) malloc(sizeof(SNode));
			table[i][j]->c = fgetc(inputFile);
			table[i][j]->codeLength = fgetc(inputFile);
			table[i][j]->code = 0;
		}
	}

	if(debugMessages == 2){
		printf("DEBUG: Table extracted\n");
		for(int i = 0; i < numDiffChars; i++){
			printf("%02x\n", diffChars[i]);
			for(int j = 0; j < numCharsInRow[i]; j++){
				printf("\t%02x: %i\n", table[i][j]->c, table[i][j]->codeLength);
			}
		}
	}

	//Reconstruct canoncial Huffman codes
	unsigned long bitSequence;

	for(int i = 0; i < numDiffChars; i++){
		bitSequence = 0;
		unsigned char l = table[i][0]->codeLength;
		for(int j = 1; j < numCharsInRow[i]; j++){
			bitSequence++;
			unsigned char length = table[i][j]->codeLength;
			while(l < length){
				bitSequence = bitSequence << 1;
				l++;
			}
			table[i][j]->code = bitSequence;
		}
	}

	if(debugMessages == 2){
		printf("DEBUG: Canonical Huffman codes reconstructed\n");
		for(int i = 0; i < numDiffChars; i++){
			printf("%c\n", diffChars[i]);
			for(int j = 0; j < numCharsInRow[i]; j++){
				printf("\t%c: %x\n", table[i][j]->c, table[i][j]->code);
			}
		}
	}

	outputFile = fopen(outputFileName, "w");
	unsigned char prev = fgetc(inputFile);
	fputc(prev, outputFile);
	fileSize--;
	unsigned char next = 0;
	unsigned char nextBits = 0;
	unsigned char numBits = 0;
	while(fileSize > 0){
		//printf("Number of Characters Remaining: %x\n", fileSize);
		//printf("\tprevious character is \"%c\"\n", prev);
		bitSequence = 0;
		unsigned char length = 0;
		int index = 0;
		for(; (index < numDiffChars) && (diffChars[index] != prev); index++);
		if(index == numDiffChars){
			printf("ERROR 1: Could not find symbol in table\n");
			return;
		}
		//printf("\tIndex is %i\n", index);
		int f = 0;
		int max = table[index][numCharsInRow[index] - 1]->codeLength;
		for(; f <= max; f++) {
			//printf("\tIn second while-loop. Bitstring is \"%s\"\n", bitString);
			//printf("\tnextBits is \"%i\" and numBits is %i\n", nextBits, numBits);
			//printf("\t%x : %i\n", bitSequence, length);
			for(int i = 0; i < numCharsInRow[index]; i++){
				//printf("\t\t%c:%x\n", table[index][i]->c, table[index][i]->code);
				if(length < table[index][i]->codeLength) continue;
				if(table[index][i]->code == bitSequence){
					next = table[index][i]->c;
					f = max + 2;
					break;
				}
			}
			if(f < max) {
				if(numBits == 0){
					nextBits = fgetc(inputFile);
					numBits = 8;
				}
				bitSequence = bitSequence << 1;
				length++;
				if(nextBits & 0x80) bitSequence++;
				nextBits = nextBits << 1;
				numBits--;
			}
		}
		if(f == (max + 1)){
			printf("ERROR 2: Could not find symbol in table\n");
			return;
		}
		fputc(next, outputFile);
		prev = next;
		fileSize--;
	}
	if(debugMessages) printf("DEBUG: Finished writing to output file\n");
	for(int i = 0; i < numDiffChars; i++){
		for(int j = 0; j < numCharsInRow[i]; j++){
			free(table[i][j]);
		}
		free(table[i]);
	}
	free(table);
	free(diffChars);
	free(numCharsInRow);
	fclose(inputFile);
	fclose(outputFile);
}