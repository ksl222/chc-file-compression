#include "functions.h"

//Flags
char c;

int main(int argc, char** argv){
	if(argc > 1){
		if(strcmp(argv[1], "-help") == 0){
			printf("Compression Usage: ./bin/exe/chc -c InputFileName\n");
			printf("-o FileName\tChanges compressed file name to FileName argument\n");
			printf("Expansion Usage: ./bin/exe/chc -e InputFileName\n");
			printf("-o Path/To/Output/Directory\tPuts the expanded file in the specified directory given by the path\n");
			return 0;
		}
	}

	if(argc < 3 || argc > 5){
		printf("Invalid number of arguments entered. For more usage help, use \"./bin/exe/chc -help\"\n");
		return 0;
	}

	//Check for compression or decompression
	if(strcmp(argv[1], "-c") == 0) c = 1;
	else if(strcmp(argv[1], "-e") == 0) c = 0;
	else{
		printf("ERROR: Invalid flag entered. Expected \"-c\" or \"-e\", but received \"%s\"\n", argv[1]);
		return 1;
	}

	//File Names
	char* inputFileName = argv[2];
	char* outputFileName = NULL;

	//Check if input file can be opened
	FILE* inputFile = fopen(inputFileName, "r");
	if(inputFile == NULL){
		printf("ERROR: Unable to open input file \"%s\"\n", inputFileName);
		return 1;
	}

	fclose(inputFile);	

	if(debugMessages){
		if(c) printf("DEBUG: Compressing input file, \"%s\"\n", inputFileName);
		else printf("DEBUG: Decompressing input file, \"%s\"\n", inputFileName);
	}

	//Check for flags in arguments entered
	if(argc > 3){
		for(int i = 3; i < argc; i++){
			if(strcmp(argv[i], "-o") == 0){
				if((argc - i) > 1){
					outputFileName = argv[++i];
					continue;
				}
				else{
					printf("ERROR: Expected output file name after -o flag, but could not find one.\n");
					return 1;
				}
			}
			else{
				printf("ERROR: Invalid argument entered, \"%s\"\n", argv[i]);
				return 1;
			}
		}
	}

	//COMPRESSION CODE
	if(c){
		char f = 0;
		if(outputFileName == NULL) {
			//outputFileName = defaultOutput;
			char fileNameSize = strlen(inputFileName);
			outputFileName = malloc(fileNameSize + 4);
			memcpy(outputFileName, inputFileName, fileNameSize);
			outputFileName[fileNameSize] = '.';
			outputFileName[fileNameSize + 1] = 'c';
			outputFileName[fileNameSize + 2] = 'h';
			outputFileName[fileNameSize + 3] = 'c';
			outputFileName[fileNameSize + 4] = 0;
			f = 1;
		}
		if(debugMessages){
			printf("DEBUG: Output file name \"%s\"\n", outputFileName);
		}

		//Check if input and output files are the same
		if(strcmp(inputFileName, outputFileName) == 0){
			printf("ERROR: Input file name cannot be the same as the ouput file name\n");
			if(f) free(outputFileName);
			return 1;
		}
		if(debugMessages) printf("DEBUG: Checked input/output file names\n");

		FILE* outputFile = fopen(outputFileName, "r");
		
		//Check whether the output file already exists
		if(outputFile != NULL){
			printf("WARNING: Output file, \"%s\", already exists. If you continue it will be overwritten.\n", outputFileName);
			fclose(outputFile);
			printf("Would you like to proceed anyway? (y/n)\t");
			int k = getc(stdin);
			if(k == 'n'){
				printf("\nCompression aborted.\n");
				if(f) free(outputFileName);
				return 0;
			}
			else if(k == 'y'){
				printf("\nCompression continuing.\n");
			}
			else{
				while((k != 'y') && (k != 'n')){
					printf("\nERROR: Invalid option selected.\n");
					printf("Would you like to proceed anyway? (y/n)\t");
					k = getc(stdin);
				}
				if(k == 'n'){
				printf("\nCompression aborted.\n");
				if(f) free(outputFileName);
				return 0;
				}
				else if(k == 'y'){
					printf("\nCompression continuing.\n");
				}
			}
		}
		if(debugMessages) printf("DEBUG: Checked whether output file already exists\n");
		compress(inputFileName, outputFileName);
		if(f) free(outputFileName);
	}
	//EXPANSION CODE
	else{
		if(outputFileName == NULL) outputFileName = (char*) malloc(0);
		expansion(inputFileName, outputFileName);
	}
}